﻿using System;
using System.Collections.Generic;
using System.Linq;

public class SortedSearch
{
    public static int CountNumbers(int[] sortedArray, int lessThan)
    {
        var ix = 0;
        sortedArray.ToList().Sort();
        foreach (var item in sortedArray)
        {
            if (item < lessThan)
            {
                ix++;
            }

        }
        return ix;
    }

}

class TwoSum
{
    public static Tuple<int, int> FindTwoSum(List<int> list, int sum)
    {

        for (var ix = 0; ix < list.Count; ix++)
        {

            for (var iy = 0; iy < list.Count; iy++)
            {
                //var tmp = list[ix];

                if (list[ix] + list[iy] == sum)
                {
                    return Tuple.Create(list[ix], list[iy]);
                }

            }


        }
        return null;
    }

    public static void Main(string[] args)
    {
        int[] number = new int[] { 1, 8, 7, 5, 4, 9 };
        number.ToList().Sort();
        int[] sorted= number;


        Tuple<int, int> indices = FindTwoSum(new List<int>() { 3, 1, 5, 7, 5, 9 }, 10);
        if (indices != null)
        {
            Console.WriteLine(indices.Item1 + " " + indices.Item2);
        }
    }
}

